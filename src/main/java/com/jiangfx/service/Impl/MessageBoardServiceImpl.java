package com.jiangfx.service.Impl;

import com.jiangfx.entity.MessageBoard;
import com.jiangfx.mapper.MessageBoardMapper;
import com.jiangfx.service.MessageBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jiangfeixiang on 2018/5/5
 */
@Service
@Transactional
public class MessageBoardServiceImpl implements MessageBoardService {

    //注入messageBoardMapper
    @Autowired
    private MessageBoardMapper messageBoardMapper;

    /**
     * 查询所有留言记录
     * @return
     */
    @Override
    public List<MessageBoard> getAllMessage() {
        List<MessageBoard> allMessage = messageBoardMapper.getAllMessage();

        return allMessage;
    }

    /**
     * 保存留言板
     * @param messageBoard
     */
    @Override
    public void saveMessage(MessageBoard messageBoard) {
        messageBoardMapper.saveProduct(messageBoard);
    }

    /**
     * 查询留言详细信息
     * @param id
     * @return
     */
    @Override
    public MessageBoard detailMessageById(Integer id) {
        MessageBoard messageBoard = messageBoardMapper.detailMessageById(id);
        return messageBoard;
    }
}

package com.jiangfx.service;

import com.jiangfx.entity.MessageBoard;

import java.util.List;

/**
 * Created by jiangfeixiang on 2018/5/3
 */
public interface MessageBoardService {
    /**
     * 查询所有留言记录
     */
    List<MessageBoard> getAllMessage();

    /**
     * 添加留言
     * @param messageBoard
     */
    void saveMessage(MessageBoard messageBoard);

    /**
     * 查询留言详细信息
     * @param id
     * @return
     */
    MessageBoard detailMessageById(Integer id);
}

package com.jiangfx.entity;

/**
 * Created by jiangfeixiang on 2018/5/5
 * 留言板实体类
 */
public class MessageBoard {
    private Integer id;
    private String username;
    private String iphone;
    private String email;
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIphone() {
        return iphone;
    }

    public void setIphone(String iphone) {
        this.iphone = iphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageBoardService{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", iphone='" + iphone + '\'' +
                ", email='" + email + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}

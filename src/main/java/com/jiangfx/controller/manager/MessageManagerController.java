package com.jiangfx.controller.manager;

import com.jiangfx.entity.MessageBoard;
import com.jiangfx.service.MessageBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by jiangfeixiang on 2018/5/5
 */
@Controller
@RequestMapping("/admin")
public class MessageManagerController {

    //注入messageBoardService
    @Autowired
    private MessageBoardService messageBoardService;
    /**
     * 查询所有留言记录
     */
    @RequestMapping(value = "/getAllMessage",method = RequestMethod.GET)
    public String messagePage(ModelMap modelMap){
        List<MessageBoard> allMessage = messageBoardService.getAllMessage();
        if (allMessage !=null){
            modelMap.addAttribute("allMessage",allMessage);
            return "manager/messageboardlist";
        }else {
            modelMap.addAttribute("error","留言空空");
            return "manager/messageboardlist";
        }
    }

    /**
     * 查看留言详细记录
     */
    @RequestMapping(value = "/detailMessage",method = RequestMethod.GET)
    public String detailMessage(Integer id,ModelMap modelMap){
        MessageBoard message = messageBoardService.detailMessageById(id);
        modelMap.addAttribute("message",message);
        return "manager/detailmessage";
    }

}

package com.jiangfx.controller;

import com.jiangfx.entity.Banner;
import com.jiangfx.entity.MessageBoard;
import com.jiangfx.service.BannerService;
import com.jiangfx.service.MessageBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by jiangfeixiang on 2018/5/5
 */
@Controller
public class MessageBoardController {

    //注入messageBoardService
    @Autowired
    private MessageBoardService messageBoardService;

    //注入bannerService
    @Autowired
    private BannerService bannerService;

    /**
     * 跳转到留言页
     */
    @RequestMapping(value = "/messageboardPage",method = RequestMethod.GET)
    public String messagePage(ModelMap modelMap){
        //查询banner
        List<Banner> banners = bannerService.getAllBanner();
        modelMap.addAttribute("banners",banners);
        return "messageboardPage";
    }

    /**
     * 保存留言内容
     */
    @RequestMapping(value = "/saveMessage",method = RequestMethod.POST)
    public String saveMessage(MessageBoard messageBoard){
        messageBoardService.saveMessage(messageBoard);
        return "success";
    }
}

<%--
  Created by IntelliJ IDEA.
  User: smfx1314
  Date: 2018/5/4
  Time: 9:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
    <meta charset="UTF-8">
    <title>留言板</title>
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css" media="screen">
        body{
            padding-top: 60px;
        }
        li:hover {
            background-color: #BEBEBE;
        }
        .carousel{
            height: 350px;
            background-color: #000;
            margin-bottom: 60px;
        }
        .carousel .item{
            height: 350px;
            background-color: #000;

        }
        .carousel img{
            width: 100%;
        }
        .carousel-caption p{
            margin-bottom: 20px;
            font-size: 20px;
            line-height: 1.8;
        }

        #summary-container .col-md-3{
            text-align: center;
            margin-bottom: 20px;
        }

        hr.divider{margin: 40px 0;}
        .feature{
            padding: 30px 0;
        }

    </style>
</head>
<body>
<!-- 导航条 -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">

        <!-- 页面缩显示 -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
            <a class="navbar-brand" href="#">现代浏览器博物馆</a>
        </div>

        <!-- 导航标签 -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class=""><a href="/">首页</a></li>
                <li><a href="#">新闻中心</a></li>
                <li class="dropdown">
                    <%--<a href="${pageContext.request.contextPath}/getProduct" class="dropdown-toggle" data-toggle="dropdown" id="product_dropdown" >产品中心 <span class="caret"></span></a>--%>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">产品中心 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#tab-chrome">Chrome</a></li>
                        <li><a href="#tab-firefox">Firefox</a></li>
                        <li><a href="#tab-safari">Safari</a></li>
                        <li><a href="#tab-opera">Opera</a></li>
                        <li><a href="#tab-ie">IE</a></li>
                    </ul>
                </li>
                <li><a href="#">留言板</a></li>
                <li><a href="#">关于我们</a></li>
            </ul>
        </div><!-- 导航标签结束-->
    </div>
</nav><!-- 导航条 -->

<!-- carousel轮播图 -->
<!-- carousel轮播图 -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- 设置图片轮播的顺序 -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <%--<li data-target="#carousel-example-generic" data-slide-to="3"></li>
        <li data-target="#carousel-example-generic" data-slide-to="4"></li>--%>
    </ol>

    <!-- 设置轮播图片 -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="${pageContext.request.contextPath}/images/firefox-big.jpg" alt="...">
            <div class="carousel-caption">
                <h1>${banner.bannerdesc}</h1>
                <p>
                    <a class="btn btn-lg btn-primary" href="" role="button" target="_blank">点击下载</a>
                </p>
            </div>
        </div>
        <c:forEach items="${banners}" var="banner">
            <div class="item">
                <img src="/image/${banner.imagepath}" alt="...">
                <div class="carousel-caption">
                    <h1>${banner.bannerdesc}</h1>
                    <p>
                        <a class="btn btn-lg btn-primary" href="" role="button" target="_blank">点击下载</a>
                    </p>
                </div>
            </div>
        </c:forEach>
    </div>

    <!-- 设置轮播图片控制器 -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<%--留言表单--%>
<div class="container">
    <form id="addPage" action="${pageContext.request.contextPath }/saveMessage" method="post">
        <div class="form-group">
            <label>姓名</label>
            <input type="text" name="username" class="form-control" placeholder="姓名">
        </div>
        <div class="form-group">
            <label>电话</label>
            <input type="text" name="iphone" class="form-control" placeholder="电话">
        </div>
        <div class="form-group">
            <label>邮箱</label>
            <input type="email" name="email" class="form-control" placeholder="邮箱">
        </div>
        <div class="form-group">
            <label>留言内容</label>
        </div>
        <textarea style="width: 800px; height: 200px;" name="message"></textarea>
        <button type="submit" class="btn btn-default">提交</button>
    </form>
</div>

</body>
</html>
